Changelog
=========

0.1.0 (2015-08-29)
------------------

1. Initial release.

0.1.1 (2015-08-29)
------------------

1. Fix missing templates during installation with pip

0.1.2 (2015-08-30)
------------------

1. Put "Global settings" in the settings submenu
2. Added site specific settings model

0.1.3 (2015-08-30)
------------------

1. Fix permission enforcement

0.1.4 (2015-08-30)
------------------

1. Check for permissions for concrete content type on edit

